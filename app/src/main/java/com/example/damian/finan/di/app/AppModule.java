package com.example.damian.finan.di.app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.example.damian.finan.App;
import com.example.damian.finan.di.scope.ApplicationScope;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by damian on 09.11.17.
 */
@Module
public class AppModule  {


    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @ApplicationScope
    public App provideApplication() {
        return app;
    }

    @Provides
    @ApplicationScope
    public Context provideContext() {
        return app;
    }

    @Provides
    @ApplicationScope
    public FirebaseDatabase provideFirebaseDatabase(){
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        return firebaseDatabase;
    }
    @Provides
    @ApplicationScope
    public FirebaseAuth provideFirebaseAuth(){
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        return firebaseAuth;
    }


}
