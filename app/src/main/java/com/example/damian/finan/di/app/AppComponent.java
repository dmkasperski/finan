package com.example.damian.finan.di.app;

import android.content.Context;
import com.example.damian.finan.App;
import com.example.damian.finan.di.scope.ApplicationScope;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;


import dagger.Component;

/**
 * Created by damian on 09.11.17.
 */

@ApplicationScope
@Component(
        modules = {AppModule.class}
)
public interface AppComponent {
    void inject (App app);
    Context context();
    App app();
    FirebaseDatabase firebaseDatabase();
    FirebaseAuth firebaseAuth();

}
