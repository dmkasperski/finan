package com.example.damian.finan;


import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.example.damian.finan.di.app.AppComponent;
import com.example.damian.finan.di.app.AppModule;
import com.example.damian.finan.di.app.DaggerAppComponent;


public class App extends Application {
    private AppComponent component;

    public AppComponent getComponent() {
        return component;
    }
@Override
    public void onCreate() {
    super.onCreate();
    this.setAppComponent();

}
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    private void setAppComponent() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }
}
