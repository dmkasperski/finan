package com.example.damian.finan.ui.login.view;



import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.damian.finan.R;
import com.example.damian.finan.ui.login.presenter.LoginPresenter;
import com.google.firebase.auth.FirebaseAuth;



import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RegisterFragment extends DialogFragment {

    @BindView(R.id.edittext_Remail)
    EditText email;

    @BindView(R.id.edittext_Rpassword)
    EditText password;
    @BindView(R.id.button_Rsignup)
    Button register;


    LoginPresenter presenter;
    FirebaseAuth mAuth;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        presenter = new LoginPresenter(getContext(), mAuth);
        View view = View.inflate(getContext(), R.layout.activity_register, null);
        ButterKnife.bind(this, view);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        return view;
    }

    @OnClick(R.id.button_Rsignup)
    void signup() {
        presenter.createAccountWithEmail(email.getText().toString(), password.getText().toString());
    }


}
