package com.example.damian.finan.ui.login.view;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.example.damian.finan.App;
import com.example.damian.finan.MainActivity;
import com.example.damian.finan.R;
import com.example.damian.finan.di.login.DaggerLoginComponent;
import com.example.damian.finan.di.login.LoginComponent;
import com.example.damian.finan.di.login.LoginModule;
import com.example.damian.finan.ui.login.LoginInterface;
import com.example.damian.finan.ui.login.presenter.LoginPresenter;
import com.example.damian.finan.utils.Network;
import com.google.android.gms.auth.api.Auth;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;

import com.google.android.gms.common.api.GoogleApiClient;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginInterface.View, GoogleApiClient.OnConnectionFailedListener {

    @Inject
    LoginPresenter presenter;

    LoginComponent component;
    private FirebaseAuth mAuth;
    @BindView(R.id.button_signin_google)
    Button signinWithGoogle;
    @BindView(R.id.button_signin)
    Button singin;
    @BindView(R.id.button_signup)
    Button sinup;
    @BindView(R.id.edittext_email)
    EditText email;
    @BindView(R.id.edittext_password)
    EditText password;

    private int GOOGLE_SIGN_IN = 5153;
    private int REGISTER = 4351;
    private GoogleApiClient mGoogleApiClient;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        FirebaseAuth.getInstance().signOut();
        if (component == null) {
            component = DaggerLoginComponent.builder()
                    .appComponent(App.get(this).getComponent())
                    .loginModule(new LoginModule(this))
                    .build();
            component.inject(this);
        }
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null ){
            startMainActivity();
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setAuthListener();
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.removeAuthListener();
    }
    @OnClick(R.id.button_signin_google)
    void signinWithGoogle() {
        if (Network.isNetworkAvailable(this)) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
        }else
            Network.NetworkIsNotAvailableMessege(this);
    }
    @OnClick(R.id.button_signin)
    void signin() {
        if(Network.isNetworkAvailable(this)) {
            if(validate()){
            presenter.singinWithEmail(email.getText().toString(), password.getText().toString());
        }}
        else
            Network.NetworkIsNotAvailableMessege(this);
    }

    @OnClick(R.id.button_signup)
    void register() {
        DialogFragment newFragment = new RegisterFragment();
        newFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

    public boolean validate(){
        boolean result = true;
        if (TextUtils.isEmpty(email.getText().toString())) {
            email.setError("Email missing!");
            result = false;
        } else {
            email.setError(null);
        }

        if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError("Password missing!");
            result = false;
        } else {
            password.setError(null);
        }

        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                presenter.singinWithGoogle(result.getSignInAccount());

            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("LOG", "onConntectionFailed " + connectionResult.getErrorMessage());
    }
}
