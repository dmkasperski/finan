package com.example.damian.finan.di.login;

import com.example.damian.finan.di.app.AppComponent;
import com.example.damian.finan.di.scope.ActivityScope;
import com.example.damian.finan.ui.login.presenter.LoginPresenter;
import com.example.damian.finan.ui.login.view.LoginActivity;

import dagger.Component;

/**
 * Created by damian on 09.11.17.
 */
@ActivityScope
@Component(
        dependencies = {AppComponent.class},
        modules = {LoginModule.class}

)
public interface LoginComponent {
    //activity
    void inject(LoginActivity view);

    //presenters
    void inject(LoginPresenter presenter);

    //dialog
   // void inject(SpotsDialog spotsDialog);

}
