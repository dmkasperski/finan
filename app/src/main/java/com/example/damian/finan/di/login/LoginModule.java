package com.example.damian.finan.di.login;

import android.content.Context;

import com.example.damian.finan.ui.login.LoginInterface;
import com.example.damian.finan.ui.login.presenter.LoginPresenter;
import com.example.damian.finan.ui.login.view.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by damian on 09.11.17.
 */
@Module
public class LoginModule {
    private LoginActivity mView;
    private Context context;

    public LoginModule(LoginActivity view){
        this.mView=view;
        this.context=view.getContext();
    }
    @Provides
    LoginInterface.View provideLoginActivity(){
        return mView;
    }

    @Provides
    LoginPresenter provideLoginPresenter(FirebaseAuth firebaseAuth){
        return new LoginPresenter(context, firebaseAuth);
    }
}
