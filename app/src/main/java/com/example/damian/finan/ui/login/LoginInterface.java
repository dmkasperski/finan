package com.example.damian.finan.ui.login;

import android.content.Context;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by damian on 07.11.17.
 */

public interface LoginInterface {
    interface View{
        void startMainActivity();
        Context getContext();
    }
    interface Presenter{
        void singinWithGoogle(GoogleSignInAccount googleSignInAccount);
        void singinWithEmail(String email, String password);
        void createAccountWithEmail(String email,String password);

    }
}
